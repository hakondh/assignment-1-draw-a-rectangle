# Draw a rectangle

---

Small program that takes user input to create a 
rectangle, using for-loops. Two nested rectangles
will be drawn if possible. 
