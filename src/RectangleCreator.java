import java.util.InputMismatchException;
import java.util.Scanner;

class RectangleCreator {
    // Constants used in multiple methods
    private static final String CONSTANT_BUILDING_BLOCK = "#";
    private static final String CONSTANT_SPACE_BLOCK = " ";

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int width;
        int height;
        char choice;

        System.out.println("Welcome to the Rectangle Creator");

        boolean run = true;
        while(run) {
            try {
                System.out.println("Enter width: ");
                width = scanner.nextInt();
                System.out.println("Enter height: ");
                height = scanner.nextInt();
                if(height < 3 || width < 3) {
                    System.out.println("Please enter width and height values higher than 3");
                }
                else {
                    drawRectangle(width, height);
                    System.out.println("Write anything to draw another rectangle, and q to quit: ");
                    choice = scanner.next().charAt(0);
                    if(choice == 'q') run = false;
                }
            }
            catch(InputMismatchException e) {
                System.out.println("Please enter only valid values");
                scanner.next(); // Clear the wrong input
            }
        }
        System.out.println("Bye!");
    }

    // Function to draw the rectangle to CLI
    private static void drawRectangle(int width, int height) {
        String filledRow = getFilledRow(width);
        String middleRow;
        boolean drawNested = width > 6 && height > 6;

        for(int i = 0; i < height; i++) {
            // Draw a filled row if we are at the top or bottom of the rectangle
            if(i == 0 || i == height - 1){
                System.out.println(filledRow);
            }
            // Draw a middle row if we are in the middle of the rectangle
            else {
                // Draw a row with a nested rectangle if possible
                if(drawNested && 1 < i && i < height - 2) {
                    // Check if the row of the nested rectangle should be a filled or middle row
                    boolean endRow = i == 2 || i == height - 3;
                    middleRow = getNestedMiddleRow(width - 4, endRow);
                }
                // If not, draw a normal row
                else {
                    middleRow = getMiddleRow(width);
                }
                System.out.println(middleRow);
            }
        }
    }

    // Get the row used for top and bottom of the rectangle
    private static String getFilledRow(int width){
        return CONSTANT_BUILDING_BLOCK.repeat(width);
    }

    // Get middle row of the rectangle
    private static String getMiddleRow(int width) {
        return CONSTANT_BUILDING_BLOCK + CONSTANT_SPACE_BLOCK.repeat(width - 2) + CONSTANT_BUILDING_BLOCK;
    }

    // Get middle row with a nested Rectangle, with either a end row or middle row
    private static String getNestedMiddleRow(int width, boolean ends) {
        String rowStart = CONSTANT_BUILDING_BLOCK + CONSTANT_SPACE_BLOCK;
        String rowMiddle = "";
        String rowEnd = CONSTANT_SPACE_BLOCK + CONSTANT_BUILDING_BLOCK;
        if(ends) rowMiddle += getFilledRow(width);
        else rowMiddle += getMiddleRow(width);
        return rowStart + rowMiddle + rowEnd;
    }
}
